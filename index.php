<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<title>Me and the boys</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
	<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body style="background: rgba(87,87,87,0.45)">
	<nav class="navbar navbar-default" style="background: #303030; border: 0px">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>                        
				</button>
				<a class="navbar-brand" href="index.php" style="color: rgba(255,255,255,0.8)">Me and the boys</a>
			</div>
			<div class="collapse navbar-collapse" id="myNavbar">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="liste.php" style="color: rgba(255,255,255,0.8)">Liste des membres</a></li>
					<li><a href="https://iteracode.fr/" target="_blank" style="color: rgba(255,255,255,0.8)">Iteracode</a></li>
					<li><a href="http://www.iut-amiens.fr/" target="_blank" style="color: rgba(255,255,255,0.8)">IUT Amiens</a></li>
				</ul>
			</div>
		</div>
	</nav>
	<div class="container-fluid text-center">
		<h3 class="margin" style="color: rgba(255,255,255,0.8)"> NightCode - 9 et 10 janvier 2020</h3>
		<div style="margin: 100px"></div>
		<img src="ezgif.com-video-to-gif.gif" class="img-responsive img-circle margin" style="display:inline;margin:20px" alt="Javelina">
	<div style="margin: 70px">
		<audio autoplay>
			<source src="AreYouWithUs.wav" type="audio/wav">
			Votre navigateur ne supporte pas la balise audio.
		</audio>
	</div>
		<img src="Blason.png" class="img-responsive margin" style="display:inline" alt="Blason">
	</div> 
</body>
</html>
