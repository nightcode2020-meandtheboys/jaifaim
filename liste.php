<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<title>Me and the boys</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
	<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body style="background: rgba(87,87,87,0.45)">
	<nav class="navbar navbar-default" style="background: #303030; border: 0px">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>                        
				</button>
				<a class="navbar-brand" href="index.php" style="color: rgba(255,255,255,0.8)">Me and the boys</a>
			</div>
			<div class="collapse navbar-collapse" id="myNavbar">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="liste.php" style="color: rgba(255,255,255,0.8)">Liste des membres</a></li>
					<li><a href="https://iteracode.fr/" target="_blank" style="color: rgba(255,255,255,0.8)">Iteracode</a></li>
					<li><a href="http://www.iut-amiens.fr/" target="_blank" style="color: rgba(255,255,255,0.8)">IUT Amiens</a></li>
				</ul>
			</div>
		</div>
	</nav>
	<div class="container">
		<h3 class="margin text-center" style="color: rgba(255,255,255,0.8); margin: 20px"> NightCode - 9 et 10 janvier 2020</h3>  
		<div style="margin: 120px"></div>
		<div class="row">
			<div class="col-sm-4">
				<div class="card text-center" style="width: 18rem;">
					<div class="card-body">
						<h5 class="card-title">Gabet Benjamin (Groupe 3)</h5>
						<img src="image_profils/1.png" class="mr-3" alt="profil">
						<p class="card-text">Créateur du GIF</p>
						<a href="https://gitlab.com/BenjaminGabet" target="_blank" class="card-link">Gitlab</a>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="card text-center" style="width: 18rem;">
					<div class="card-body">
						<h5 class="card-title">Louvet Quentin (Groupe 3)</h5>
						<img src="image_profils/2.png" class="mr-3" alt="profil">
						<p class="card-text">Le second boss en JSON</p>
						<a href="https://gitlab.com/QuentinLouvet" target="_blank" class="card-link">Gitlab</a>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="card text-center" style="width: 18rem;">
					<div class="card-body">
						<h5 class="card-title">Sobolewski Louis-Baptiste (Groupe 3)</h5>
						<img src="image_profils/3.png" class="mr-3" alt="profil">
						<p class="card-text">Le boss en JSON</p>
						<a href="https://gitlab.com/louis-baptiste.sobolewski" target="_blank" class="card-link">Gitlab</a>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="card text-center" style="width: 18rem;">
					<div class="card-body">
						<h5 class="card-title">Jacoillot Victor (Groupe 4)</h5>
						<img src="image_profils/4.png" class="mr-3" alt="profil">
						<p class="card-text">Page d'accueil</p>
						<a href="https://gitlab.com/victor_jacoillot" target="_blank" class="card-link">Gitlab</a>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="card text-center" style="width: 18rem;">
					<div class="card-body">
						<h5 class="card-title">Hasle Clément (Groupe 3)</h5>
						<img src="image_profils/5.png" class="mr-3" alt="profil">
						<p class="card-text">Createur du Blason</p>
						<a href="https://gitlab.com/Clement_Hasle" target="_blank" class="card-link">Gitlab</a>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="card text-center" style="width: 18rem;">
					<div class="card-body">
						<h5 class="card-title">Salvetti Gabriel (Groupe 3)</h5>
						<img src="image_profils/6.png" class="mr-3" alt="profil">
						<p class="card-text">Animateur de gags</p>
						<a href="https://gitlab.com/GabrielSalvetti" target="_blank" class="card-link">Gitlab</a>
					</div>
				</div>
			</div>
			<div class="col-sm-12">
				<div class="card text-center" style="width: 18rem;">
					<div class="card-body">
						<h5 class="card-title">Boulogne Bastian (Groupe 1)</h5>
						<img src="image_profils/7.png" class="mr-3" alt="profil">
						<p class="card-text">Createur d'affiche</p>
						<a href="https://gitlab.com/nait-sab" target="_blank" class="card-link">Gitlab</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>