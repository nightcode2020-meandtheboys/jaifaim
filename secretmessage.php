<?php
    function reduce($texte)
    {
        $new_chaine = "";
        $texte = $texte . " ";
        for ($indice = 1; $indice < strlen($texte); $indice++){
            if ($texte[$indice - 1] != $texte[$indice]){
                $new_chaine = $new_chaine.$texte[$indice - 1];
            }
            else
                $indice++;
        }
        
        $doublons = false;
        for ($indice = 1; $indice < strlen($new_chaine); $indice++){
            if ($new_chaine[$indice - 1] != $new_chaine[$indice]){
                $doublons = true;
            }
        }
        if($doublons) 
            reduce($new_chaine);
        else
            return $new_chaine;
    }
?>