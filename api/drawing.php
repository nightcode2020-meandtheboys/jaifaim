<?php
$rqBody = file_get_contents("php://input");
$json = json_decode($rqBody, TRUE);
$matrice = $json["payload"];

function testMatrix($matrix)
{
    $isMatrixCorrect = TRUE;

    if(count($matrix) == 0)
    {
        $isMatrixCorrect = FALSE;
    }
    else
    {
        $length = count($matrix[0]);

        foreach($matrix as $row)
        {
            if(count($row) != $length)
            {
                $isMatrixCorrect = FALSE;
            }
        }
    }

    return $isMatrixCorrect;
}

if(testMatrix($matrice))
{
    $img = imagecreatetruecolor(count($matrice), count($matrice[0])); 
    foreach($matrice as $x => $col)
    {
        foreach($col as $y => $row)
        {
            switch($row)
            {
                case 0:
                    $color =  imagecolorallocate($img, 255, 255, 255);
                    break;
                case 1:
                    $color =  imagecolorallocate($img, 0, 0, 0);
                    break;
            }
            imagesetpixel($img, $x, $y, $color);
        }
    }
    
    ob_start();
    imagepng($img);
    $imagedata = ob_get_clean();
    
    echo('{"image":"data:image/png;base64,' . base64_encode($imagedata) . '"}');
    header('content-type:application/json');
}
else
{
    http_response_code(400);
}

?>