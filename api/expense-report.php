<?php
    $data = file_get_contents("php://input");
    
    $result = 0;

    $isCorrect = TRUE;
    
    if(preg_match("/[a-z]/i", $data)){
        http_response_code(400);
        $isCorrect = FALSE;
    }

    function postfix($calcul)
    {
        $split = explode(" ", $calcul);
        $sommet = -1;
        $pile = NULL;

        foreach($split as $element){
            if($element == '+'){
                $pile[$sommet - 1] =  $pile[$sommet] + $pile[$sommet - 1];
                $sommet--;
            }
            else
                if($element == '-'){
                    $pile[$sommet - 1] =  $pile[$sommet - 1] - $pile[$sommet];
                    $sommet--; 
                }
                else{
                    $sommet++;
                    $pile[$sommet] = (int)$element;
                }
        }

        if($sommet==-1)
            return NULL;
        return $pile[0];
    }

    if($isCorrect)
    {
        if($_GET["type"] != "postfix"){
            $result_plus = explode(" + ", $data);
    
            foreach($result_plus as &$value_plus){
                $value_plus = strtok($value_plus, ' ');
            }
    
            $result_minus = explode(" - ", $data);
    
            foreach($result_minus as &$value_minus){
                $value_minus = strtok($value_minus, ' ');
            }
    
            array_shift($result_minus);
    
            foreach($result_plus as &$value_plus){
                $result += $value_plus;
            }
    
            foreach($result_minus as &$value_minus){
                $result -= $value_minus;
            }
    
            echo('{"value":' . $result . '}');
            header('content-type:application/json');
        }
        else{
            $result = postfix($data);
            if($result == NULL){
                http_response_code(400);
            }
            else{
                echo('{"value":' . $result . '}');
                header('content-type:application/json');
            }
            
        }
    }
?>