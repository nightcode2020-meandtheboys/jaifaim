<?php
$rqBody = file_get_contents("php://input");
$message = "";

if($rqBody != "") //il y a du JSON
{
    $json = json_decode($rqBody, TRUE);

    if($json["name"] == "") //bad rq
    {
        $message = NULL;
    }
    else
    {
        $message = $json["name"];
    }
}
else if(!empty($_GET)) //c'est en GET
{
    if(!isset($_GET["name"])) //bad rq
    {
        $message = NULL;
    }
    else
    {
        $message = $_GET["name"];
    }
}
else //c'est le message par défaut
{
    $message = "world";
}

if($message == NULL) //bad rq
{
    http_response_code(400);
}
else
{
    echo('{"message":"hello, ' . $message . '"}');
    header('content-type:application/json');
}
?>
