<?php
function reduce($texte)
{
    $new_chaine = "";
    $texte = $texte . " ";
    for ($indice = 1; $indice < strlen($texte); $indice++){
        if ($texte[$indice - 1] != $texte[$indice]){
            $new_chaine = $new_chaine.$texte[$indice - 1];
        }
        else
            $indice++;
    }
    
    return $new_chaine;
}

$rqBody = file_get_contents("php://input");
$badRq = FALSE;
$badMethod = FALSE;
$translation = "";

if($_SERVER["REQUEST_METHOD"] === "POST")
{
    $json = json_decode($rqBody, TRUE);
    
    if(isset($json["to-translate"]) != TRUE) //bad rq
    {
        $badRq = TRUE;
    }
    else
    {
        $translation = reduce($json["to-translate"]);
    }
}
else //c'est du get
{
    $badMethod = TRUE;
}

if($badRq)
{
    http_response_code(400);
}
else if($badMethod)
{
    http_response_code(405);
}
else
{
    echo('{"text":"' . $translation . '"}');
    header('content-type:application/json');
}
?>