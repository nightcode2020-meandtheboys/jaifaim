<?php
$rqBody = file_get_contents("php://input");
$translation = "";
$badRq = FALSE;
$badMethod = FALSE;

if($_SERVER["REQUEST_METHOD"] === "POST")
{
    $json = json_decode($rqBody, TRUE);
    
    if(isset($json["to-translate"]) != TRUE) //bad rq
    {
        $badRq = TRUE;
    }
    else
    {
        $translation = base64_decode($json["to-translate"]);
    }
}
else //c'est en get
{
    $badMethod = TRUE;
}

if($badRq)
{
    http_response_code(400);
}
else if($badMethod)
{
    http_response_code(405);
}
else
{
    echo('{"text":"' . $translation . '"}');
    header('content-type:application/json');
}
?>